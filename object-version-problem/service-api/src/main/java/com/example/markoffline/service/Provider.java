/*
 * Copyright (2013): Author: Mark Richards (mark.alan.richards@gmail.com)
 * Copyright is retained by Mark Richards.
 * 
 * The licence to this copy is provided under the provisions:
 *  - the recipient does not redistribute to others
 *  - use for commercial or production purposes beyond
 *  - execution of compilations are to learn and evaluate the code
 *  - this licence notice must not be removed.
 */
package com.example.markoffline.service;
public interface Provider<T> {
	public T provide();
}
