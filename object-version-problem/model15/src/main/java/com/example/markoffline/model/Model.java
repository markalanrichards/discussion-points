/*
 * Copyright (2013): Author: Mark Richards (mark.alan.richards@gmail.com)
 * Copyright is retained by Mark Richards.
 * 
 * The licence to this copy is provided under the provisions:
 *  - the recipient does not redistribute to others
 *  - use for commercial or production purposes beyond
 *  - execution of compilations are to learn and evaluate the code
 *  - this licence notice must not be removed.
 */
package com.example.markoffline.model;

public class Model {
	private long date;
	private String description;
	public long getDate() {
		return date;
	}
	public void setDate(long date) {
		this.date = date;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Override
	public String toString() {
		return "Date: " + date 
				+ "\nDescription: " + description;
	}
}
