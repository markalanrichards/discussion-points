/*
 * Copyright (2013): Author: Mark Richards (mark.alan.richards@gmail.com)
 * Copyright is retained by Mark Richards.
 * 
 * The licence to this copy is provided under the provisions:
 *  - the recipient does not redistribute to others
 *  - use for commercial or production purposes beyond
 *  - execution of compilations are to learn and evaluate the code
 *  - this licence notice must not be removed.
 */
package com.example.markoffline.consumer;

import java.util.Collection;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import com.example.markoffline.model.Model;
import com.example.markoffline.service.Provider;

public class ModelConsumer implements BundleActivator {
	public void consume(Model model1) {
		System.out.println("TO STRING: " + model1.toString());
		System.out.println("THE LONG: " + model1.getDate());
	}

	@Override
	public void start(BundleContext context) throws Exception {
		System.out.println("finding refs");
		Collection<ServiceReference<Provider>> providerRefs = context.getServiceReferences(Provider.class,null);
		for(ServiceReference<Provider> ref : providerRefs) {
			for(String s : ref.getPropertyKeys()) {
				System.out.println(s + ": " + ref.getProperty(s).toString());
			}
			
			Model model = (Model) context.getService(ref).provide();
			consume(model);
		}
		
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		// TODO Auto-generated method stub
		
	}

	
}
