/*
 * Copyright (2013): Author: Mark Richards (mark.alan.richards@gmail.com)
 * Copyright is retained by Mark Richards.
 * 
 * The licence to this copy is provided under the provisions:
 *  - the recipient does not redistribute to others
 *  - use for commercial or production purposes beyond
 *  - execution of compilations are to learn and evaluate the code
 *  - this licence notice must not be removed.
 */
package com.example.markoffline.model;

import java.util.Date;

public class Model {
	private Date date;
	private String description;
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Override
	public String toString() {
		return "Date: " + date.toString() 
				+ "\nDescription: " + description;
	}
}
