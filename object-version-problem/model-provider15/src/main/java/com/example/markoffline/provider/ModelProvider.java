/*
 * Copyright (2013): Author: Mark Richards (mark.alan.richards@gmail.com)
 * Copyright is retained by Mark Richards.
 * 
 * The licence to this copy is provided under the provisions:
 *  - the recipient does not redistribute to others
 *  - use for commercial or production purposes beyond
 *  - execution of compilations are to learn and evaluate the code
 *  - this licence notice must not be removed.
 */
package com.example.markoffline.provider;

import java.util.Hashtable;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

import com.example.markoffline.model.Model;
import com.example.markoffline.service.Provider;

public class ModelProvider implements BundleActivator, Provider<Model>{
	ServiceRegistration<Provider> reg = null;
	@Override
	public Model provide() {
		Model model = new Model();
		model.setDate(System.currentTimeMillis());
		model.setDescription("The time now");
		return model;
	}

	@Override
	public void start(BundleContext context) throws Exception {
		Hashtable<String, Object> props = new Hashtable<String, Object>();
		props.put("provides", Model.class.getName());
		reg = context.registerService(Provider.class, this, props);
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		reg.unregister();
		reg = null;
	}
	
}
